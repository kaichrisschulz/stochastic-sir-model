# Stochastic SIR-Model

This repository contains the program code I used to create the figures and graphs in my bachelor thesis "Grundlagen der mathematischen Epidemiologie - Analyse und Variationen des SIR-Modells".

SIRsol.mlx:
Matlab code for chapters III-V in my thesis. The file should be opened as a "Live Script" in Matlab. It contains plotting of numerical solutions of the SIR-model and numerical simulations for the SIR-model under uncertain/random initial conditions and parameters.

R-Wert.R:
R program, displays the development of the 4- and 7-day reproduction number in Germany, based on the published numbers of the Robert-Koch-Institut.
