Nowcasting = read.csv2("D:\\Aachen\\Mathe\\6.Semester\\Bachelorarbeit\\Materialien\\Nowcasting.csv",
                 header=TRUE)



now4 = Nowcasting$Punktsch�.tzer.der.4.Tage.R.Wert
now4 = gsub(",",".", now4)
now4 = as.double(now4)
now4 = na.omit(now4)
now4 = now4[-441]

now7 = Nowcasting$Punktsch�.tzer.des.7.Tage.R.Wertes
now7 = gsub(",",".",now7)
now7 = as.double(now7)
now7 = na.omit(now7)

length(now7)
length(now4)
now4
now7

par(cex=1.2)

plot(1:440, now4, type="l", col="blue", xlim=c(0,440), ylim=c(0.5,3.5), lwd=2, xaxt="n",
     xlab="Datum", ylab="R-Wert", main="Zeitlicher Verlauf des 4- und 7-Tage-R-Wert Punktsch�tzers")
axis(side=1, at=c(0,100,200,301,400,440), labels=c("06/03/20","14/06/20","22/09/20","01/01/21","10/04/21","19/05"))
lines(1:440, now7, col="red",lwd=2)
abline(h=1)
legend("right",legend=c("4-Tage-R-Wert Punktsch�tzer", "7-Tage-R-Wert Punktsch�tzer"),
       col=c("blue","red"), lty=c(1,1), lwd=c(2,2) )

